import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Registro de usuarios';
  mensaje = '';
  registrado = false;
  
  nombre: string = '';
  apellido: string = '';

  entradas: any[];

  constructor() {
    this.entradas = [
      { titulo: 'Python cada día más presente' },
      { titulo: 'Java presente hace más de 20 años' },
      { titulo: 'JavaScript cada vez más funcional' },
      { titulo: 'Kodlen potencia para tus apps' },
      { titulo: '¿Donde quedó pascal?' },
    ];
  }

  registrarUsuario() {
    this.registrado = true;
    this.mensaje = 'Usuario registrado con éxito';
  }
}
